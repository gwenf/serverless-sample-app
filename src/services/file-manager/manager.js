'use strict';
require('dotenv').config();

module.exports.retrieveFile = (event, context, cb) => cb(null,
  { message: 'I got the file. Yay!', event }
);

module.exports.confirmFile = (event, context, cb) => cb(null,
  { message: 'File uploaded successfully. Yay!', event }
);

module.exports.fileRequested = (event, context, cb) => cb(null,
  { message: 'Somebody requested a file. Nice!', event }
);
