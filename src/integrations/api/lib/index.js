'use strict';

const AWS = require('aws-sdk');

const Lambda = class Lambda {
  constructor() {
    // TODO - Add apiVersion to config.
    this.lambda = new AWS.Lambda({ apiVersion: '2015-03-31' });
  }

  invokeFunction(arn, data, async) {
    return new Promise((resolve, reject) => {
      if (typeof data !== 'object' && typeof data !== 'string') {
        console.log(typeof data);
        return reject({ message: 'data provided is invalid.', data });
      }

      const params = {
        FunctionName: arn,
        InvocationType: async ? 'Event' : 'RequestResponse',
        Payload: typeof data === 'object' ? JSON.stringify(data) : data,
      };

      this.lambda.invoke(params, (error, result) => {
        if (error) {
          reject(error);
        } else {
          console.log(result);
          resolve(result);
        }
      });
    });
  }
};

const Sns = class SNS {
  constructor() {
    this.sns = new AWS.SNS({ apiVersion: '2010-03-31' });
  }

  sendMessage(message, topic) {
    return new Promise((resolve, reject) => {
      const params = {
        Message: JSON.stringify(message),
        TopicArn: topic,
      };

      this.sns.publish(params, (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
};

module.exports = { Lambda, Sns };
