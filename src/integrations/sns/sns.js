'use strict';
require('dotenv').config();
const lib = require('./lib');

module.exports.confirmFile = (event, context, cb) => {
  const stage = process.env.STAGE;
  const region = process.env.REGION;

  const functionName = `${region}-${stage}-file-manager-fileRequested`;
  lib.invokeFunction(functionName, { event }, false)
  .then((result) => {
    cb(null, result);
  })
  .catch((error) => {
    cb(new Error(`[500] ${error}`), null);
  });
};
