args=("$@")

function install {
  (
    cd $1;\
    npm install --silent "${args[0]}";\
  )
}

npm install --silent "${args[0]}";

for dir in ./src/integrations/*; do
  if [[ -d "$dir" ]]; then
    install $dir
  fi
done

for dir in ./src/services/*; do
  if [[ -d "$dir" ]]; then
    install $dir
  fi
done